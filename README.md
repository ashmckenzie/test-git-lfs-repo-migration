## Test Git LFS migration

References:

- Similar guide on BitBucket: https://confluence.atlassian.com/bitbucket/use-bfg-to-migrate-a-repo-to-git-lfs-834233484.html
- For https://gitlab.com/gitlab-org/gitlab-ce/issues/58911
- BFG instructions: https://rtyley.github.io/bfg-repo-cleaner/

## Steps

### 1. Clone `--mirror` the repo

Cloning with the mirror flag will create a bare repository. This ensures you get all the branches within the repo.

It creates a directory called `<repo-name>.git`, mirroring the upstream project.

Command:

```bash
git clone --mirror git@gitlab.com:gitlab-tests/test-git-lfs-repo-migration.git
```

Response:

```
Cloning into bare repository 'test-git-lfs-repo-migration.git'...
remote: Enumerating objects: 19, done.
remote: Counting objects: 100% (19/19), done.
remote: Compressing objects: 100% (18/18), done.
remote: Total 19 (delta 3), reused 0 (delta 0)
Receiving objects: 100% (19/19), 2.98 MiB | 1.68 MiB/s, done.
Resolving deltas: 100% (3/3), done.
```

### 2. Back it up

Create a copy of your repo's local dir so that you can recover it in case something goes wrong.

### 3. Install BFG and Git LFS

Command:

```bash
brew install bfg
```

Command:

```bash
brew install git-lfs
```

### 4. Convert the Git history with BFG

Command:

```bash
bfg --convert-to-git-lfs "*.{png,mp4,jpg,gif}" --no-blob-protection test-git-lfs-repo-migration.git
```

Response:

```bash

Using repo : /Users/ramosmd/GitLab/test-git-lfs-repo-migration.git

Found 0 objects to protect
Found 2 commit-pointing refs : HEAD, refs/heads/master

Protected commits
-----------------

You're not protecting any commits, which means the BFG will modify the contents of even *current* commits.

This isn't recommended - ideally, if your current commits are dirty, you should fix up your working copy and commit that, check that your build still works, and only then run the BFG to clean up your history.

Cleaning
--------

Found 3 commits
Cleaning commits:       100% (3/3)
Cleaning commits completed in 169 ms.

Updating 1 Ref
--------------

  Ref                 Before     After
  ---------------------------------------
  refs/heads/master | aa595864 | 708be559

Updating references:    100% (1/1)
...Ref update completed in 18 ms.

Commit Tree-Dirt History
------------------------

  Earliest      Latest
  |                  |
     D     D      D

  D = dirty commits (file tree fixed)
  m = modified commits (commit message or parents changed)
  . = clean commits (no changes to file tree)

                          Before     After
  -------------------------------------------
  First modified commit | 45936e73 | c583d0ef
  Last dirty commit     | aa595864 | 708be559

Changed files
-------------

  Filename                      Before & After
  -------------------------------------------------
  docs-labels-workflows.png   | 11f6f556 ⇒ 9968c19b
  docs-labels.png             | d7209202 ⇒ 926b46d5
  pages_force_https_v12_0.png | b9e7f70f ⇒ a03d2433
  screenshot-reposize.png     | e6e47d33 ⇒ b61f7670


In total, 8 object ids were changed. Full details are logged here:

  /Users/ramosmd/GitLab/test-git-lfs-repo-migration.git.bfg-report/2019-07-10/17-54-48

BFG run is complete! When ready, run: git reflog expire --expire=now --all && git gc --prune=now --aggressive


--
You can rewrite history in Git - don't let Trump do it for real!
Trump's administration has lied consistently, to make people give up on ever
being told the truth. Don't give up: https://www.aclu.org/
--

```

### 5. Clean up the repo

```bash
cd path/to/mirror/repo
git reflog expire --expire=now --all && git gc --prune=now --aggressive
```

### 6. Init Git LFS in the mirror repo

Command:

```bash
git lfs install
```

Response:

```bash
Updated git hooks.
Git LFS initialized.
```

### 7. Track the files with LFS

Commands:

```bash
git lfs track "*.gif" "*.png" "*.jpg" "*.psd" "*.mp4" ".gitattributes"
```

### 8. Unprocted the default branch

Navigate to your project's **Settings > Repository** and expand **Protected Branches**. Scroll down to locate the protected branches and click **Unprotect** the default branch, otherwise you can't force push to that branch.

### 9. Force-push to GitLab

Command:

```bash
git push --force
```

Response:

```bash
Locking support detected on remote "origin". Consider enabling it with:
  $ git config lfs.https://gitlab.com/gitlab-tests/test-git-lfs-repo-migration.git/info/lfs.locksverify true
Uploading LFS objects: 100% (4/4), 258 KB | 0 B/s, done
Enumerating objects: 20, done.
Counting objects: 100% (20/20), done.
Delta compression using up to 12 threads
Compressing objects: 100% (18/18), done.
Writing objects: 100% (20/20), 2.77 MiB | 168.00 KiB/s, done.
Total 20 (delta 3), reused 7 (delta 1)
To gitlab.com:gitlab-tests/test-git-lfs-repo-migration.git
 + aa59586...708be55 master -> master (forced update)
```
